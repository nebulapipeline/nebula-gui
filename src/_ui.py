'''
This file contains common reusable gui components or widgets
'''
# gui imports
# TODO: remove PySide because it's common

from Qt.QtWidgets import (QApplication, QMessageBox, QFrame, QLabel,
                          QGraphicsDropShadowEffect, QPushButton,
                          QMenu, QWidgetAction, QCheckBox, QAction)
from Qt.QtCore import Qt, Signal
from Qt.QtCompat import loadUi

# python imports
import os

# construct commonly used paths
root_path = os.path.dirname(os.path.dirname(__file__))
icon_path = os.path.join(root_path, 'icons')
ui_path = os.path.join(root_path, 'ui')


__all__ = ['MultiComboBox', 'StatusDialog', 'SearchBox', 'getMayaWindow',
           'showMessage', 'styleSheet', 'getPathsFromFileDialogResult']


class MultiComboBox(QFrame):
    '''
    This class offers comboBox to select multiple objects at once
    '''
    selectionDone = Signal(list)

    def __init__(self, parent=None, msg='--Select--', triState=False):
        super(MultiComboBox, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'multicombo.ui'), self)

        self.triState = triState
        self.msg = msg
        self.menu = QMenu(self)
        self.menu.setMinimumWidth(100)
        self.menu.setStyleSheet("QMenu { menu-scrollable: 1; }")
        self.button.setMenu(self.menu)

        self.menu.hideEvent = self.menuHideEvent
        self.setHintText(self.msg)

    def addDefaultWidgets(self):
        button = QPushButton('Invert Selection', self)
        button.clicked.connect(self.invertSelection)
        checkableAction = QWidgetAction(self.menu)
        checkableAction.setDefaultWidget(button)
        self.menu.addAction(checkableAction)
        btn = self.addItem('Select All')
        btn.clicked.connect(lambda: self.toggleAll(btn.checkState()))
        self.menu.addSeparator()

    def invertSelection(self):
        for cBox in self.getWidgetItems():
            cBox.setChecked(not cBox.isChecked())
        self.toggleSelectAllButton()

    def toggleAll(self, state):
        for cBox in self.getWidgetItems():
            cBox.setCheckState(state)

    def toggleSelectAllButton(self):
        flag = True
        for cBox in self.getWidgetItems():
            if not cBox.isChecked():
                flag = False
                break
        for action in self.menu.actions():
            if type(action) == QAction:
                continue
            widget = action.defaultWidget()
            if widget.text() == 'Select All':
                widget.setChecked(flag)

    def setHintText(self, text):
        self.msg = text
        self.button.setText(text)

    def menuHideEvent(self, event):
        items = self.getSelectedItems()
        if items:
            s = ''
            if len(items) > 2:
                s = ',...'
            self.button.setText(','.join(items[:2]) + s)
        else:
            self.setHintText(self.msg)
        if event:
            QMenu.hideEvent(self.menu, event)
        self.selectionDone.emit(items)

    def getSelectedItems(self):
        return [
            cBox.text().strip() for cBox in self.getWidgetItems()
            if cBox.isChecked()
        ]

    def getState(self):
        return {
            cBox.text().strip(): cBox.checkState()
            for cBox in self.getWidgetItems()
        }

    def getWidgetItems(self):
        return [
            cBox for cBox in [
                action.defaultWidget() for action in self.menu.actions()
                if not type(action) is QAction
            ] if cBox.text().strip() != 'Select All'
            and cBox.text().strip() != 'Invert Selection'
        ]

    def getItems(self):
        return [cBox.text().strip() for cBox in self.getWidgetItems()]

    def addItem(self, item, selected=False):
        checkBox = QCheckBox(item, self.menu)
        checkBox.setTristate(self.triState)
        if selected:
            checkBox.setChecked(True)
        checkableAction = QWidgetAction(self.menu)
        checkableAction.setDefaultWidget(checkBox)
        self.menu.addAction(checkableAction)
        return checkBox

    def appendItems(self, items, selected=None):
        self.addItems(items, selected=selected, clear=False)

    def addItems(self, items, selected=None, clear=True):
        items = sorted(items)
        if clear:
            self.clearItems()
        self.addDefaultWidgets()
        for item in items:
            sel = False
            if selected and item in selected:
                sel = True
            btn = self.addItem(item, sel)
            btn.clicked.connect(self.toggleSelectAllButton)
        if selected:
            self.menuHideEvent(None)

    def clearSelection(self):
        self.toggleAll(False)

    def clearItems(self):
        self.menu.clear()
        self.setHintText(self.msg)
        self.toggleSelectAllButton()


class StatusDialog(QLabel):
    def __init__(self, parent=None):
        super(StatusDialog, self).__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Dialog)
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(200, 100)
        effect = QGraphicsDropShadowEffect()
        effect.setBlurRadius(5)
        self.setGraphicsEffect(effect)
        self.setAlignment(Qt.AlignCenter)
        self.setFrameShape(QFrame.StyledPanel)
        self.setMinimumSize(200, 100)
        self.setMargin(6)

    def setStatus(self, status):
        self.setText(status)
        self.adjustSize()


class SearchBox(QFrame):
    def __init__(self, parent, item_list):
        super(SearchBox, self).__init__(parent)
        loadUi(os.path.join(ui_path, 'searchBox.ui'), self)
        self._item_list = item_list # widgets with show and hide method
        path = os.path.join(icon_path, 'search.png').replace('\\', '/')
        style = ("""padding-left: 15px; background-image: url('%s');
                background-repeat: no-repeat; background-position: center left;
                border-width: 1px; border-style: inset; border-color: #535353;
                border-radius: 9px; padding-bottom: 1px;""") % path
        self.searchBox.setStyleSheet(style)

        self.searchBox.textChanged[str].connect(self.search_items)
        self.searchBox.returnPressed.connect(lambda: self.search_items(
            self.searchBox.text()))

    def search_items(self, text):
        sources = str(text).split()
        for _item in self._item_list:
            target = _item.searchableString().lower()
            if not sources or any([
                True if src.lower() in target else False
                    for src in sources]):
                _item.show()
            else:
                _item.hide()

    def clear(self):
        self.searchBox.clear()

    @property
    def textChanged(self):
        return self.searchBox.textChanged

    @property
    def returnPressed(self):
        return self.searchBox.returnPressed

    def text(self):
        return self.searchBox.text()

    def setText(self, text):
        self.searchBox.setText(text)

    def clear(self):
        self.searchBox.clear()


def getMayaWindow():
    try:
        for obj in QApplication.topLevelWidgets():
            if obj.objectName() == 'MayaWindow':
                return obj
    except Exception:
        pass


class _MessageBox(QMessageBox):
    '''
    Creates a custom message box to show orbitrary messages
    '''

    def __init__(self, parent=None):
        super(_MessageBox, self).__init__(parent)

    def closeEvent(self, event):
        self.deleteLater()


def showMessage(parent,
                title='Message',
                msg='Message',
                btns=QMessageBox.Ok,
                icon=None,
                ques=None,
                details=None,
                **kwargs):

    mBox = _MessageBox(parent)
    mBox.setWindowTitle(title)
    mBox.setText(msg)
    if ques:
        mBox.setInformativeText(ques)
    if icon:
        mBox.setIcon(icon)
    if details:
        mBox.setDetailedText(details)
    customButtons = kwargs.get('customButtons')
    mBox.setStandardButtons(btns)
    if customButtons:
        for btn in customButtons:
            mBox.addButton(btn, QMessageBox.AcceptRole)
    pressed = mBox.exec_()
    if customButtons:
        cBtn = mBox.clickedButton()
        if cBtn in customButtons:
            return cBtn
    return pressed


# some usefull CSS
borderColor = '#252525'
flat = '\nborder-style: solid;\nborder-color: ' + \
    borderColor + ';\nborder-width: 1px;\nborder-radius: 0px;\n'
styleSheet = ('QComboBox {' + flat + '\nmin-height: 25;\nmin-width: 125}' +
              'QPushButton {' + flat + '\nheight: 23;\nwidth: 75;\n}\n' +
              'QPushButton:hover, QToolButton:hover ' +
              '{\nbackground-color: #353535;\nborder-style: ' +
              'solid;\nborder-color: #4876FF\n}' + 'QLineEdit {height: 23;' +
              flat + '}\n'
              'QToolButton {' + flat + '}' + 'QPlainTextEdit {' + flat + '}')


def getPathsFromFileDialogResult(result):
    if result and isinstance(result, tuple):
        return result[0]
    return result
