'''
This module contains interface (abstract) classes
'''
import abc
# TODO: remove PySide because it's common
from Qt.QtCore import QObject, Signal


__all__ = ['ISearchableItem', 'Intermediate', 'IJob']


class ISearchableItem(object):
    __metaclass__ = abc.ABCMeta
    '''
    abstract base class for searchable items
    '''
    @abc.abstractmethod
    def searchableString(self):
        '''
        returns a searchable string (e.g item title, subtitle)
        '''
        pass


# abc.ABCMeta because ISearchableItem's metaclass is abc.ABCMeta
class Intermediate(abc.ABCMeta, type(QObject)):
    '''
    a class to avoid metaclass conflics
    '''


class IJob(QObject):
    # interface for defining a worker to update ui
    __metaclass__ = Intermediate

    update_ui_signal = Signal()

    STATUS_WAITING = 0
    STATUS_WORKING = 1
    STATUS_DONE = 2

    status = STATUS_WAITING

    def __init__(self):
        super(IJob, self).__init__()
        self.update_ui_signal.connect(self.update)

    @abc.abstractmethod
    def perform(self):
        # load the data from database and show on ui
        pass

    @abc.abstractmethod
    def update(self):
        pass
